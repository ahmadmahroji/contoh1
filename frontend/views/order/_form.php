<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\multiselect\MultiSelect;

/* @var $this yii\web\View */
/* @var $model common\models\OrderCustomer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-customer-form">

    <?php $form = ActiveForm::begin(); ?>

   <?= $form->field($model, 'items')->widget(MultiSelect::className(),[
	    'data' => $items,
	    "options" => ['multiple'=>"multiple"],
	]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
