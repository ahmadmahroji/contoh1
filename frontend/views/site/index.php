<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;


$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            
            <?php foreach ($provider->getModels() as $i => $item) { ?>
                <div class="col-md-3" style="margin-bottom : 50px;">
                    <center>
                        <?php if($item->pic != null){ ?>
                            <?= Html::img(Yii::$app->urlImage->baseUrl. "/" .$item->pic, [
                                "width" => '100',
                                "height" => '100',
                             ]) ?>
                     <?php }else{ ?>
                                 <?= Html::img(Yii::$app->urlImage->baseUrl. "/noImage.png", [
                                "width" => '100',
                                "height" => '100',
                             ]) ?>
                         <?php } ?>

                        <p><?= $item->name ?> <br/> <?= $item->price ?></p>

                        <?php if(!Yii::$app->user->isGuest){ ?>
                            <p><a class="btn btn-success" href="/">Beli</a></p>
                        <?php } ?>
                    </center>
                </div>
            <?php } ?>
        </div>
        <center>
            <?= LinkPager::widget([
                'pagination' => $provider->pagination,
            ]) ?>
        </center>
    </div>
</div>
