<?php

namespace frontend\models;

use Yii;
use yii\base\model;
use common\models\OrderItem;

class OrderForm extends Model{
	public $orderId;
	public $items;

	public function rules(){
		return[
			['orderId', 'integer'],
			['orderId', 'safe'],
			['items', 'each', 'rule' => ['integer']],
		];
	}
	public function save(){
		$flag = true;
		foreach ($this->items as $i => $idItem) {
			$model = new OrderItem();
			$model->order_id =$this->orderId;
			$model->item_id = $idItem;

			if(!$model->save()){
				$flag = false;
			}
		}
		return $flag;
	}
}