<?php

namespace backend\models;

use Yii;
use yii\base\model;
use yii\web\UploadedFile;
use common\models\Item;

class ItemForm extends Model{
	public $name;
	public $price;
	public $category;
	public $pic;

    public function rules()
    {
        return [
        	[['name', 'price', 'category'], 'required'],
        	['name', 'string', 'max' => 255],
        	[['price', 'category'], 'integer'],
        	['pic', 'safe'],

            [['pic'], 'file', 'extensions' => 'png, jpg'],
        ];
    }
    public function save(){
    	if(!$this->validate()){
    		return false;
    	}
    	$image = UploadedFile::getInstance($this, 'pic');
    	if($image){
    		$randomName = Yii::$app->getSecurity()->generateRandomString(32)
    			.".".$image->extension;
    		$path = Yii::getAlias('@image')."/".$randomName;
    		$image->saveAs($path);
    		$image->name = $randomName;
    	}

    	$model = new Item();
    	$model->name = $this->name;
    	$model->price = $this->price;
    	$model->category_id = $this->category;
    	if($image){
    		$model->pic = $image->name;
    	}

    	if($model->save()){
    		return $model->id;

    	}else{
    		return false;
    	}
    }
}